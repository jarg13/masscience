var express = require('express');
var app = express();

app.use(express.static('Angular_Full_Version'));

var server = app.listen(9092, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('En marcha angularjs escuchando en http://%s:%s', host, port);

});
